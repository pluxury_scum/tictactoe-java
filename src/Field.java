public class Field {
    public char[][] fieldArea;
    public int movesCounter;

    public Field() {
        this.fieldArea = new char[3][3];
        for (var verticalIndex = 0; verticalIndex < 3; verticalIndex++) {
            for (var horizontalIndex = 0; horizontalIndex < 3; horizontalIndex++) {
                this.fieldArea[verticalIndex][horizontalIndex] = ' ';
            }
        }
        this.movesCounter = 0;
    }

    public boolean isCellNotEmpty(int verticalIndex, int horizontalIndex) {
        return !(this.fieldArea[verticalIndex][horizontalIndex] == ' ');
    }

    public void showField() {
        System.out.print("\n  ");
        for (int horizontalIndex = 0; horizontalIndex < 3; horizontalIndex++) {
            System.out.print(" " + horizontalIndex);
        }
        System.out.println();
        for (var verticalIndex = 0; verticalIndex < 3; verticalIndex++) {
            System.out.print(verticalIndex + " ");
            for (var horizontalIndex = 0; horizontalIndex < 3; horizontalIndex++) {
                System.out.print("|" + this.fieldArea[verticalIndex][horizontalIndex]);
            }
            System.out.println("|");
        }

        System.out.println();
    }

    public boolean isGameFinished() {
        if ((this.fieldArea[0][0] == this.fieldArea[1][1]) && (this.fieldArea[1][1] == this.fieldArea[2][2]) &&
                (this.fieldArea[0][0] != ' '))
            return true;
        else if ((this.fieldArea[0][2] == this.fieldArea[1][1]) && (this.fieldArea[1][1] == this.fieldArea[2][0]) &&
                (this.fieldArea[0][2] != ' '))
            return true;
        else if ((this.fieldArea[0][0] == this.fieldArea[0][1]) && (this.fieldArea[0][1] == this.fieldArea[0][2]) &&
                (this.fieldArea[0][0] != ' '))
            return true;
        else if ((this.fieldArea[0][1] == this.fieldArea[1][1]) && (this.fieldArea[1][1] == this.fieldArea[2][0]) &&
                (this.fieldArea[0][1] != ' '))
            return true;
        else if ((this.fieldArea[1][0] == this.fieldArea[1][1]) && (this.fieldArea[1][1] == this.fieldArea[1][2]) &&
                (this.fieldArea[1][0] != ' '))
            return true;
        else if ((this.fieldArea[2][0] == this.fieldArea[2][1]) && (this.fieldArea[2][1] == this.fieldArea[2][2]) &&
                (this.fieldArea[2][0] != ' '))
            return true;
        else if ((this.fieldArea[0][0] == this.fieldArea[1][0]) && (this.fieldArea[1][0] == this.fieldArea[2][0]) &&
                (this.fieldArea[0][0] != ' '))
            return true;
        else if ((this.fieldArea[0][1] == this.fieldArea[1][1]) && (this.fieldArea[1][1] == this.fieldArea[1][2]) &&
                (this.fieldArea[0][1] != ' '))
            return true;
        else if ((this.fieldArea[0][2] == this.fieldArea[1][2]) && (this.fieldArea[1][2] == this.fieldArea[2][2]) &&
                (this.fieldArea[0][2] != ' '))
            return true;
        return false;
    }

    public void getWinner(int movesCounter) {
        char winnerSymbol = (movesCounter % 2 == 0) ? 'O' : 'X';
        System.out.println("Congratulations! " + winnerSymbol + " player won");
    }
}
