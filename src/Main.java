import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Field field = new Field();
        Actions actionHandler = new ActionHandler();
        Scanner in = new Scanner(System.in);
        boolean isGameActive = true;

        while (isGameActive) {
            field.showField();
            field.movesCounter++;
            char fillerSymbol = (field.movesCounter % 2 == 0) ? 'O' : 'X';
            System.out.print(fillerSymbol + ", enter vertical index: ");
            int verticalIndex = in.nextInt();
            System.out.print(fillerSymbol + ", enter horizontal index: ");
            int horizontalIndex = in.nextInt();
            actionHandler.fillCell(field.movesCounter, field, verticalIndex, horizontalIndex);
            if (field.isGameFinished()) {
                field.getWinner(field.movesCounter);
                isGameActive = false;
            }
        }
    }
}
